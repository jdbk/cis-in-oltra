# Rychlý lab
## Kontrola nastavení
  * Připojte se na TMUI bigip-01, admin:Ingresslab123

![bigip01-tmui](/images/bigip01-tmui.png)
  * Zkontrolujte VS v cis-crd partition:

![bigip01-vs](/images/bigip01-vs.png)
## Vytvoření nové konfigurace
  * spusťte si VS Code z client vm:

![client-vscode](/images/client-vscode.png)
  * v levé části je adresářová struktura, jděte do oltra/use-cases/cis-examples/cis-crd/VirtualServer/TLS-Termination/

![client-tlsfolder](/images/client-tlsfolder.png)
  * projděte edge-tls.yml a edge-vs.yml
    * edge-tls.yml v kuberentes vytvoří definici ssl profilu
    * edge-vs.yml obsahuje definici virtual serveru, která odkazuje na dříve vytvořený tls profil
  * vytvořte objekty v k8s - ve spodní části obrazovky přepněte na TERMINAL:
  
![client-teminal](/images/client-terminal.png)
  * v terminálu je potřeba jít do stejného adresáře jako v prohlížeči:
```
cd oltra/use-cases/cis-examples/cis-crd/VirtualServer/TLS-Termination/
```
  * vytvořte edge tls profile:
```
kubectl apply -f edge-tls.yml
```
  * zkontrolujte že existuje:
```
kubectl get tlsprofile
```
  * vytvořte virtual server:
```
kubectl apply -f edge-vs.yml
```
  * zkontrolujte že existuje:
```
kubectl get virtualserver
```
  * zkontrolujte, že existuje v bigip01:
![bigip01-edgevs](/images/bigip01-edgevs.png)
  * kontrola funkčnosti z terminálu:
```
echo "10.1.10.68 edge.f5demo.local" | sudo tee -a /etc/hosts
curl -k  https://edge.f5demo.local 
```
### Enjoy!
![beer](/images/beer.jpeg)
