# Detailní lab
## Kontrola nastavení
### bigip01
  * Připojte se na TMUI bigip-01, admin:Ingresslab123

![bigip01-tmui](/images/bigip01-tmui.png)
  * Zkontrolujte VS v cis-crd partition:

![bigip01-vs](/images/bigip01-vs.png)
  * již existuje integrace mezi k8s a bigip na síťové úrovni
    * v kubernetes je použitý [Calico](https://github.com/projectcalico/calico) CNI (Container Network Interface), založený na BGP
    * [návod](https://clouddocs.f5.com/containers/latest/userguide/calico-config.html) na integraci s bigip
    * v Network -> Route Domains -> 0 je povolené BGP
    * konfigurace BGP je v dostupná přes SSH nebo WebShell

![bigip01-webshell](/images/bigip01-webshell.png)
```
imish
```
```
enable
```
```
show ip bgp
```
### kubernetes
  * připojte se na webshell nebo ssh master nodu

![master-webshell](/images/master-webshell.png)
```
calicoctl get nodes
```
```
calicoctl get bgpPeer
```
    * dynamický routing v kubernetes ví o bigip a bigip vi o iverlay síti v rámci kubernetes
    * bigip tak může směrovat provoz na interní adresy nodů v kubernetes clusteru
  * spusťte si VS Code z client vm:

![client-vscode](/images/client-vscode.png)
  * ve spodní části obrazovky přepněte na TERMINAL:
  
![client-teminal](/images/client-terminal.png)
  * aby bylo možné předat konfiguraci z kuberentes na bigip musí běžet [CIS](https://clouddocs.f5.com/containers/latest/) node
  * v adresářové struktuče v levé části obrazovky jděte do oltra/setup/cis

![client-vscode-cis](/images/client-vscode-cis.png)
  * pro CIS musí existovat role v k8s clusteru, která mu dá práva číst konfiguraci - soubor cis-cluster-role.yml
  * musí existovat definice sevice accountu, který bude využíván CIS podem - soubor cis-service-account.yml
  * role musí být navázána na uživatele nebo objekt v rámci clusteru, tzv. cluster role binging - soubor cis-cluster-role-binding.yml
    * v tomto případě dává práva service accountu bigip-ctlr v namespace bigip k rolu bigip-ctrl-clusterrole:

![k8s-crb](/images/k8s-crb.png)
  * typicky se vytváří [secret](https://kubernetes.io/docs/concepts/configuration/secret/), který obsahuje přístupové údaje k bigip a v k8s db je uložen v bezpečné formě
    * soubor bigip-secret.yml
    * kontrola: 
```
kubectl get secret -n bigip
```
```
kubectl describe secrets bigip-login -n bigip
```

  * služby budeme publikovat pomocí [custom resource definition](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) které umožňují rozšířit kubernetes API o vlastní definice
    * soubor cis-crd.yml
    * kontrola že jsou nainstalované:
```
kubectl api-resources
```
```
externaldnses                     edns               cis.f5.com/v1                          true         ExternalDNS
ingresslinks                      il                 cis.f5.com/v1                          true         IngressLink
policies                          plc                cis.f5.com/v1                          true         Policy
tlsprofiles                       tls                cis.f5.com/v1                          true         TLSProfile
transportservers                  ts                 cis.f5.com/v1                          true         TransportServer
virtualservers                    vs                 cis.f5.com/v1                          true         VirtualServer
```
    * tedy můžeme vytvářet tyto objekty nad rámec bežných možností v kubernetes
  * nakonec je potřeba nasadit CIS, definice je v souboru cis-ctlr-crd.yml
  
![cis-ctlr.png](/images/cis-ctlr.png)
  * soubory lze aplikovat pomocí `kubectl apply -f [nazev souboru]`
  * kontrola, že cis běží:
```
kubectl get po,svc -n bigip
```
![cis pod](/images/cis-pod.png)
  * revize použité konfigurace:
```
kubectl describe po [nazev podu z předchozího commandu] -n bigip
```
  * bigip je integrované s k8s, stačí vytvořit objekty.


## Vytvoření nové konfigurace
  * z VS code se podívejte co za služby běží v kubernetes:
```
kubectl get services
```
![get svc](/images/get-svc.png)
  * můžeme publikovat například echo-svc
  * v adresářové struktuře jděte do oltra/use-cases/cis-examples/cis-crd/VirtualServer/TLS-Termination/

![client-tlsfolder](/images/client-tlsfolder.png)
  * projděte edge-tls.yml a edge-vs.yml
    * edge-tls.yml v kuberentes vytvoří definici ssl profilu
    * edge-vs.yml obsahuje definici virtual serveru, která odkazuje na dříve vytvořený tls profil
  
![client-teminal](/images/client-terminal.png)
  * v terminálu je potřeba jít do stejného adresáře jako v prohlížeči:
```
cd oltra/use-cases/cis-examples/cis-crd/VirtualServer/TLS-Termination/
```
  * vytvořte edge tls profile:
```
kubectl apply -f edge-tls.yml
```
  * zkontrolujte že existuje:
```
kubectl get tlsprofile
```
  * vytvořte virtual server:
```
kubectl apply -f edge-vs.yml
```
  * zkontrolujte že existuje:
```
kubectl get virtualserver
```
  * zkontrolujte, že existuje v bigip01:
![bigip01-edgevs](/images/bigip01-edgevs.png)
  * kontrola funkčnosti z terminálu:
```
echo "10.1.10.68 edge.f5demo.local" | sudo tee -a /etc/hosts
curl -k  https://edge.f5demo.local 
```
  * když změníme počet replik pro echo-svc dojde k vytvoření nových podů a jejich zavedení do bigip jako pool memeberů:
  * v oltra/setup/apps/my-echo.yml změňte repicas z 2 na např. 5
```
cd ~/oltra/setup/apps/
kubectl apply -f my-echo.yml
```
  * zkontrolujte konfiguraci na bigip
### Přiřazení profilů, iRule, WAF..
  * VS má základní konfiguraci bez optimalizace, security..
  * profily a spol se přiřazují přes [policy](https://clouddocs.f5.com/containers/latest/userguide/crd/policy.html)
  * v adresářové struktuře jděte do oltra/use-cases/cis-examples/cis-crd/VirtualServer/TLS-Termination/
  * vytvořte soubor edge-policy.yml:
```
apiVersion: cis.f5.com/v1
kind: Policy
metadata:
  labels:
    f5cr: "true"
  name: edge-policy
spec:
  l7Policies:
    waf: /Common/basic_waf_policy
  profiles:
    http: /Common/http-xff
    logProfiles:
      - /Common/Log all requests
  l7Policies:
    waf: /Common/basic_waf_policy
  profiles:
    http: /Common/http-xff
    http2: /Common/http2
    logProfiles:
      - /Common/Log all requests
```
  * v edge-vs.yml přidejte policy:
```
policyName: edge-policy
```
![add policy](/images/addpolicy.png)
  * vytvořte policy a změňte VS:
```
cd ~/oltra/use-cases/cis-examples/cis-crd/VirtualServer/TLS-Termination/
kubectl apply -f edge-policy.yml 
kubectl apply -f edge-vs.yml 
```
  * zkontrolujte konfiguraci na f5 a funkčnost aplikace

```
apiVersion: cis.f5.com/v1
kind: VirtualServer
metadata:
  labels:
    f5cr: "true"
  name: edge-tls-vs
spec:
  host: edge.f5demo.local
  tlsProfileName: edge-tls
  virtualServerAddress: 10.1.10.68
  virtualServerName: "edge-tls-vs"
  policyName: edge-policy
  pools:
  - path: /
    service: echo-svc
    servicePort: 80
```
